=======
History
=======

0.1.7 (2021-04-02)
------------------

* Added Datacore v5 support


0.1.6 (2020-12-30)
------------------

* Moved to GitLab
* Updated docs
* Improved filename searches in P4Ks
* Dataforge records can now be outputed to xml as well
* Improved pretty printing of XML output

0.1.5 (2020-12-9)
-----------------

* Improved path and error handling

0.1.3 (2020-12-06)
------------------

* Added SC profile dumping (actionmaps)
* New `StarCitizen` class convenience wrapper around the installation dir
* Support for looking up localization strings
* Dataforge fixes

0.1.2 (2020-05-20)
------------------

* Initial commit
